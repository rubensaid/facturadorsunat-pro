<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        App\Models\System\User::create([
            'name' => 'Ruben Felix',
            'email' => 'ruben@tukuy.club',
            'password' => bcrypt('?StartUp2060+'),
        ]);
    }
}
